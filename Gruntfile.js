module.exports = function () {
  this.loadTasks("build/tasks");
  return this.registerTask("default", [
    "clean",
    "jshint",
    "processhtml",
    "copy",
    "requirejs",
    "styles",
    "cssmin",
    "connect",
    "watch"
  ]);
};
