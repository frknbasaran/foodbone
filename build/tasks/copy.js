module.exports = function () {
  this.loadNpmTasks("grunt-contrib-copy");
  return this.config("copy", {
    release: {
      files: [
        {
          src: "bower_components/**",
          dest: "dist/"
        },
        {
          expand: true,
          cwd: "app/assets/",
          src: "**",
          dest: "dist/"
        },
        {
          src: "favicon.ico",
          dest: "dist/"
        }
      ]
    }
  });
};
