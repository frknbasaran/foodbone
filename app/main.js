require(['bugsnag', 'backbone', 'app', 'libraries/router'],
  function (Bugsnag, Backbone, app, Router) {
    Bugsnag.apiKey = 'af83348024d415430e496a4ac291104f';
    app.router = new Router();
    Backbone.history.start({pushState: true, root: app.root});
  }
);
