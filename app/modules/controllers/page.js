define(function (require, exports, module) {
  var RegisterView = require('../views/register');
  var ErrorView = require('../views/error');
  var HomeView = require('../views/home');
  var SearchView = require('../views/search');
  var FoodView = require('../views/food');
  var VerifyView = require('../views/verify');
  var SmsView = require('../views/sms');
  var AddressView = require('../views/address');
  var SummaryView = require('../views/summary');
  var FinishView = require('../views/finish');
  var OrdersView = require('../views/orders');
  var LoginView = require('../views/login');

  module.exports = {
    'register': function () {
      var registerView = new RegisterView();

      return registerView;
    },
    'error': function () {
      var errorView = new ErrorView();

      return errorView;
    },
    'home': function () {
      var homeView = new HomeView();

      return homeView;
    },
    'search': function () {
      var searchView = new SearchView();

      return searchView;
    },
    'food': function () {
      var foodView = new FoodView();

      return foodView;
    },
    'verify': function () {
      var verifyView = new VerifyView();

      return verifyView;
    },
    'sms': function () {
      var smsView = new SmsView();

      return smsView;
    },
    'address': function () {
      var addressView = new AddressView();

      return addressView;
    },
    'summary': function () {
      var summaryView = new SummaryView();

      return summaryView;
    },
    'finish': function () {
      var finishView = new FinishView();

      return finishView;
    },
    'orders': function () {
      var ordersView = new OrdersView();

      return ordersView;
    },
    'login': function () {
      var loginView = new LoginView();

      return loginView;
    }
  };
});
