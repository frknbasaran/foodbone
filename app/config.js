require.config({
  paths: {
    'underscore': '../bower_components/lodash/dist/lodash.underscore',
    'lodash': '../bower_components/lodash/dist/lodash',
    'template': '../bower_components/lodash-template-loader/loader',
    'jquery': '../bower_components/jquery/dist/jquery',
    'backbone': '../bower_components/backbone/backbone',
    'jquery.cookie': '../bower_components/jquery.cookie/jquery.cookie',
    'jquery.noty': '../bower_components/noty/js/noty/packaged/jquery.noty.packaged',
    'nprogress': '../bower_components/nprogress/nprogress',
    'bugsnag': '../bower_components/bugsnag/src/bugsnag',
    'backbone.middleware': '../bower_components/backbone.middleware/backbone.middleware',
    'tost': '../bower_components/tost/tost.min',
    'app': './app',
    'utils': './libraries/utils',
    'cache': './libraries/cache'
  },

  lodashLoader: {
    ext: '.tpl'
  },

  deps: ['main'],

  shim: {
    'backbone': {
      deps: ['underscore', 'jquery']
    },
    'jquery.cookie': {
      deps: ['jquery']
    },
    'jquery.noty': {
      deps: ['jquery']
    },
    'template': {
      deps: ['lodash']
    },
    'backbone.middleware': {
      deps: ['backbone']
    }
  },

  config: {
    'modules/models/base': {
      apiUrl: 'http://localhost:3000'
    },
    'modules/collections/base': {
      apiUrl: 'http://localhost:3000'
    },
    'utils': {
      defaultToken: 'GETLUCKY' // if u lucky, get your lucky!
    },
    'modules/views/globals/content': {
      pageLoadTimeout: 250 // if u need
    }
  }
});
